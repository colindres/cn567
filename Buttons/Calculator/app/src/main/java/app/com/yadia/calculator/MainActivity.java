package app.com.yadia.calculator;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import static android.R.id.button1;
import static android.R.id.list;

public class MainActivity extends AppCompatActivity {

    Button one, two, three, four, five, six, seven, eight, nine, zero, equal, multiply, divide,
    subtract, add, dot, clear;

    int num = 0;

    boolean addButton = false;

    ArrayList<Integer> array1 = new ArrayList<>();
    ArrayList<Integer> array2 = new ArrayList<>();

    String TAG = "MainActivity";

    TextView tv01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv01 = (TextView) findViewById(R.id.textView);

        zero = (Button) findViewById(R.id.btn0);
        one = (Button) findViewById(R.id.btn1);
        two = (Button) findViewById(R.id.btn2);
        three = (Button) findViewById(R.id.btn3);
        four = (Button) findViewById(R.id.btn4);
        five = (Button) findViewById(R.id.button5);
        six = (Button) findViewById(R.id.btn6);
        seven = (Button) findViewById(R.id.btn7);
        eight = (Button) findViewById(R.id.btn8);
        nine = (Button) findViewById(R.id. btn9);
        equal = (Button) findViewById(R.id.btnEqual);
        multiply = (Button) findViewById(R.id.btnMult);
        divide = (Button) findViewById(R.id.btnDiv);
        subtract = (Button) findViewById(R.id.btnMin);
        add = (Button) findViewById(R.id.btnAdd);
        dot = (Button) findViewById(R.id.btnDot);
        clear = (Button) findViewById(R.id.btnClear);

        zero.setOnClickListener(new MyClickListener());
        one.setOnClickListener(new MyClickListener());
        two.setOnClickListener(new MyClickListener());
        three.setOnClickListener(new MyClickListener());
        four.setOnClickListener(new MyClickListener());
        five.setOnClickListener(new MyClickListener());
        six.setOnClickListener(new MyClickListener());
        seven.setOnClickListener(new MyClickListener());
        eight.setOnClickListener(new MyClickListener());
        nine.setOnClickListener(new MyClickListener());
        add.setOnClickListener(new MyClickListener());
        equal.setOnClickListener(new MyClickListener());
        clear.setOnClickListener(new MyClickListener());

        //TODO: Implement display number add show  and numbers  = show
        // only work with +
        // + uses listener class

    }

    public void mathFunc(int inputNumber){

        num = num + inputNumber;

        if (addButton == false)
            array1.add(inputNumber);
            tv01.setText(array1.toString());
            Log.i(TAG, Integer.toString(inputNumber) +  " Inside Array 1");


        if(addButton == true)
            array2.add(inputNumber);
            tv01.setText(array2.toString());
            Log.i(TAG, Integer.toString(inputNumber) +  " Inside Array 2");


    }

    public int clearFunc(){

        tv01.setText("0");
        array1.clear();
        array2.clear();
        addButton = false;
        return num = 0;

    }

    private class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btn0:
                    mathFunc(0);
                    break;
                case R.id.btn1:
                    mathFunc(1);
                    break;
                case R.id.btn2:
                   mathFunc(2);
                    break;
                case R.id.btn3:
                    mathFunc(3);
                    break;
                case R.id.btn4:
                    mathFunc(5);
                    break;
                case R.id.button5:
                    mathFunc(5);
                    break;
                case R.id.btn6:
                   mathFunc(6);
                    break;
                case R.id.btn7:
                   mathFunc(7);
                    break;
                case R.id.btn8:
                    mathFunc(8);
                    break;
                case R.id.btn9:
                    mathFunc(9);
                    break;
                case R.id.btnAdd:
                    addButton = true;
                    break;
                case R.id.btnEqual:
                    tv01.setText(Integer.toString(num));
                    break;
                case R.id.btnClear:
                    clearFunc();
                    break;
            }

        }
    }

}
