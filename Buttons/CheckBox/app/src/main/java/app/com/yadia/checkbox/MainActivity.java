package app.com.yadia.checkbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CheckBox cb01, cb02, cb03;
    TextView textview, textView02;
    Button btn;
    RadioButton rb01, rb02, rb03;
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cb01 = (CheckBox) findViewById(R.id.checkbox1);
        cb02 = (CheckBox) findViewById(R.id.checkbox2);
        cb03 = (CheckBox) findViewById(R.id.checkbox3);
        textview = (TextView) findViewById(R.id.textview01);
        textView02 = (TextView) findViewById(R.id.textview02);
        btn = (Button) findViewById(R.id.button01);
        rb01 = (RadioButton) findViewById(R.id.radio01);
        rb02 = (RadioButton) findViewById(R.id.radio02);
        rb03 = (RadioButton) findViewById(R.id.radio03);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = " ";

                if (cb01.isChecked())
                    str = str + cb01.getText().toString();

                if (cb02.isChecked())
                    str = str + cb02.getText().toString();

                if (cb03.isChecked())
                    str = str + cb03.getText().toString();

                textview.setText(str);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rb01.getId() == checkedId)
                    textView02.setText(rb01.getText());

                if (rb02.getId() == checkedId)
                    textView02.setText(rb02.getText());

                if (rb03.getId() == checkedId)
                    textView02.setText(rb03.getText());
            }
        });
    }
}
