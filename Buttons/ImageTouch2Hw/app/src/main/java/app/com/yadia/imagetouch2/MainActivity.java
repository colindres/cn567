package app.com.yadia.imagetouch2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.path;
import static android.R.attr.y;
import static android.os.Build.VERSION_CODES.M;
import static android.view.View.X;
import static android.view.View.Y;

public class MainActivity extends AppCompatActivity {

    TextView tv01;
    ImageView iv01;
    FrameLayout frameLayout;

    float x1 = 0;
    float x2 = 0;
    float y1 = 0;
    float y2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv01 = (TextView) findViewById(R.id.textView01);
        iv01 = (ImageView) findViewById(R.id.imageView01);
        frameLayout = (FrameLayout) findViewById(R.id.framelayout);

        int[] location = new int[2];
        iv01.getLocationOnScreen(location);
        int offsetX = location[0];
        int offsetY = location[1];

        /*
        Add two additional gestures: circling(O) and crossing(X).
        The circling and crossing gestures is performed inside the picture.
        Two additional pictures should be added.
         */


        frameLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int act = event.getAction();

                int left = iv01.getLeft();
                int right = iv01.getRight();
                int bottom = iv01.getBottom();
                int top = iv01.getTop();

                float x = event.getX();
                float y = event.getY();

                tv01.setText( "Coordinates "+"X: "+ x+" Y: "+ y + " " + MotionEvent.actionToString(act));

                switch (act){
                    case MotionEvent.ACTION_DOWN:
                        //first
                        x2 = x;
                        y2 = y;

                        //outside touch point of the picture
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // final float dx = x - x2;
                        // final float dy = y - y2;
                        //  x1 += dx;
                       // y2 += dy;

                       if(x > 460 && y > 328)
                           iv01.setImageResource(R.drawable.pc2);

                        if(x < 460 && y > 328)
                            iv01.setImageResource(R.drawable.pc3);

                        if(x < 460 && y < 328)
                            iv01.setImageResource(R.drawable.pc4);

                        if (x > 460 && y < 328)
                            iv01.setImageResource(R.drawable.pc5);

                        break;
                    case MotionEvent.ACTION_UP:
                        //last point outside picture

                        break;
                    case (MotionEvent.ACTION_OUTSIDE) :
                        return true;
                }

                return true;
            }
        });
    }
}
