package app.com.yadia.chartingapp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import static android.R.attr.data;

public class MainActivity extends AppCompatActivity {

    LineChart linechart01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linechart01 = (LineChart) findViewById(R.id.linechart01);
        setLinechartUI(linechart01);
        setData();
       // linechart01.setDescription(getString(R.string.linechart01_txt));
        Legend lineChart01_legend = linechart01.getLegend();
        lineChart01_legend.setPosition(Legend.LegendPosition.ABOVE_CHART_RIGHT);
        lineChart01_legend.setForm(Legend.LegendForm.LINE);


    }

    public void setData(){
        ArrayList<String> xValues = setXAxisValues(); //gets values from this function
        // create entries
       // ArrayList<Entry> entryArrayList = new ArrayList<Entry>();
        ArrayList<Entry> entryArrayList = setYAxisValues();

        LineDataSet dataSet1;
        dataSet1 = new LineDataSet(entryArrayList, "DataSet 01");
        dataSet1.setFillAlpha(110);
        //add UI properties to the line
        dataSet1.setColor(Color.BLACK);
        dataSet1.setCircleColor(Color.BLACK);
        dataSet1.setLineWidth(1f);
        dataSet1.setCircleRadius(3f);
        dataSet1.setDrawCircleHole(false);
        dataSet1.setValueTextSize(9f);
        dataSet1.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet1); //adds data to the LineDataSet Array

        LineData data = new LineData(dataSets);
        linechart01.setData(data);
    }

    private LineChart setLinechartUI(LineChart linechart){
        linechart.setBackgroundColor(getResources().getColor(R.color.colorGraphBackground));
        linechart.setNoDataText(getString(R.string.graph_noData));
        linechart.setNoDataTextColor(getResources().getColor(R.color.colorNoDataText));
        linechart.setDrawGridBackground(true);
        linechart.setTouchEnabled(true);
        linechart.setDragEnabled(true);
        linechart.setScaleEnabled(true);
        linechart.setHighlightPerDragEnabled(true);
        linechart.setPinchZoom(true);
        return linechart;
    }

    public void onValueSelected(Entry e, Highlight h, LineChart lineChart) {
        Log.i("Entry selected", e.toString());
        lineChart.centerViewToAnimated(e.getX(), e.getY(), lineChart.getData().getDataSetByIndex(h.getDataSetIndex())
                .getAxisDependency(), 500);
    }

    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");

        return xVals;
    }

    // This is used to store Y-axis values
    private ArrayList<Entry> setYAxisValues(){
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(60, 0));
        yVals.add(new Entry(48, 1));
        yVals.add(new Entry(70.5f, 2));
        yVals.add(new Entry(100, 3));
        yVals.add(new Entry(180.9f, 4));

        return yVals;
    }

}
