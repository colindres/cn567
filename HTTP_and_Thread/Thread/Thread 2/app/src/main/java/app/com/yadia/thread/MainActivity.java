package app.com.yadia.thread;



import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    Handler mHandler = new Handler(new HCallback());
    String TAG = "MainActivity.class";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread t1 = new Thread(new TRClass("inside the Runnable thread"));
        t1.start();

        Toast.makeText(this, "in main Thread", Toast.LENGTH_SHORT).show();
    }
    //DEFINE RUNNABLE CLASS
     class TRClass implements Runnable{
        String strText;

        TRClass(String strText){
            this.strText = strText;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);

            }catch (InterruptedException e){
                Log.i(TAG, e.toString());

            }
            Log.w("MyString", strText);
            //Toast.makeText(MainActivity.this, "In!!", Toast.LENGTH_SHORT).show();
            mHandler.obtainMessage(1, "You--->" + strText).sendToTarget();
        }
    }

    //Handler call class
    class HCallback implements Handler.Callback{

        @Override
        public boolean handleMessage(Message msg) {
            Toast.makeText(MainActivity.this, (String) msg.obj, Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
