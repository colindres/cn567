package app.com.yadia.thread;



import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
    Handler mHandler = new Handler(new HCallback());
    String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread t1 = new Thread(new TRClass("inside the Runnable thread"));
        t1.start();

        Toast.makeText(this, "in main Thread", Toast.LENGTH_SHORT).show();
    }
    //DEFINE RUNNABLE CLASS
     class TRClass implements Runnable{
        String strText;

        TRClass(String strText){
            this.strText = strText;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);

            }catch (InterruptedException e){
                Log.i(TAG, e.toString());

            }
            Log.w("MyString", strText);
            //Toast.makeText(MainActivity.this, "In!!", Toast.LENGTH_SHORT).show();
            mHandler.obtainMessage(1, "You--->" + strText).sendToTarget();
        }
    }

    //Handler call class
    class HCallback implements Handler.Callback{

        @Override
        public boolean handleMessage(Message msg) {
            Toast.makeText(MainActivity.this, (String) msg.obj, Toast.LENGTH_SHORT).show();
            return false;
        }
    }
/*
    private String sendPostDataToInternet(String strTxt){
        String uriApi = "http://58.86.139:8752/Android/Test/API/Post/index.php";

        //HTTP post OBJECT
        HttpPost httpRequest = new HttpPost(uriApi);
        //POST KEY
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("str", strText);
        try{
            httpRequest.setEntity(new URLEncodedEntity(params.HTTPUTF_8));
            if(httpResponse.getStatusLine().getStatusCode()==200){
                String strResult = EntityUtils.toString(httpResponse.getEntity()."UTF-8");
                return strResult;
            }

        }catch (UnsupportedEncodingException e){
            e.printStackTrace();

        }catch (ClientProtocol Exception e){
            e.printStackTrace();
        } catch (IOEXception e){
            e.printStackTrace();
        }
        return null;
    }
    */


    //Needs to run on Async task

    /**
     *
     * @param url  -- > http://58.86.139:8752/Android/Test/API/Post/index.php
     * @return  string from url
     * @throws IOException
     */
    private String makeHttpRequest(URL url) throws IOException{
        String strTxt = "";
        HttpURLConnection urlConnection = null;
        String line;
        StringBuffer buffer = new StringBuffer();


        if(url == null){
            return  strTxt;
        }

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            //urlConnection.setReadTimeout(1000);
            urlConnection.setConnectTimeout(1500);
            urlConnection.connect();

            //get response code
            if(urlConnection.getResponseCode() == 200){
                //inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                //keep reading till line is finished
                while((line = bufferedReader.readLine()) != null){
                    //add buffer read data to strtTxt
                }
            }
        } catch (IOException e){
            e.printStackTrace();
            return  null;
        } finally {
            urlConnection.disconnect();
        }
        return strTxt;
    }
}
