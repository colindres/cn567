package app.com.yadia.hw05;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnsave, btnTag, btnEdit, btnClear;
    EditText et_query, et_tag;
    String query, tag;
    LinearLayout ll;
    boolean buttonCreated = false;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_query = (EditText) findViewById(R.id.et_query);
        et_tag = (EditText) findViewById(R.id.et_tag);
        btnsave = (Button) findViewById(R.id.btn_save);
        ll = (LinearLayout) findViewById(R.id.linearlayout01);
        btnClear = (Button) findViewById(R.id.btnClear);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAll();
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 btnTag = new Button(MainActivity.this);
                 btnEdit = new Button(MainActivity.this);

                if(!buttonCreated){
                    tag = et_tag.getText().toString();
                    query = et_query.getText().toString();

                    if (tag.matches("")|| query.matches("") ){
                        Toast.makeText(MainActivity.this, "You need to name your tags and query",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        btnTag.setWidth(210);
                        btnTag.setText(tag);
                        btnTag.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                runQuery(query);
                            }
                        });

                        btnEdit.setText("Edit");
                        btnEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                editQuery(btnTag);
                            }
                        });
                        ll.addView(btnTag);
                        ll.addView(btnEdit);

                        et_tag.getText().clear();
                        et_query.getText().clear();
                        buttonCreated = true;
                    }

                } else {
                    tag = et_tag.getText().toString();
                    query = et_query.getText().toString();

                    btnTag.setText(tag);
                    btnTag.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                    et_tag.getText().clear();
                    et_query.getText().clear();
                }
            }
        });
    }

    private void runQuery(String myquery){
        Intent intent = new Intent(MainActivity.this, QueryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("QUERY", query);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void editQuery(Button btn){
        et_tag.setText(btn.getText());
        et_query.setText(query);

        if (et_query.getText() != null) {
            query = et_query.getText().toString();
            tag = et_tag.getText().toString();
        }
    }

    private void clearAll(){
        query = "";
        tag = "";
        ll.removeAllViews();
        buttonCreated = false;
        Toast.makeText(MainActivity.this, "Tags all cleared", Toast.LENGTH_SHORT);
    }

}
