package app.com.yadia.hw05;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QueryActivity extends AppCompatActivity {
    
    String query, urlQuery;
    Button btnSearch;
    EditText et_query;
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        btnSearch = (Button) findViewById(R.id.btn_search2);
        webview = (WebView) findViewById(R.id.myWebView);

        et_query= (EditText) findViewById(R.id.tv_searchQuery);

        Bundle bundle = this.getIntent().getExtras();
        query = bundle.getString("QUERY");
        et_query.setText(query);

        webview.setWebViewClient(new MyWebView());
        runSearch(query);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query = "";
                query = et_query.getText().toString();
                runSearch(query);
            }
        });

    }

    private void runSearch(String myquery){
        urlQuery = "https://www.google.com/search?q=" + myquery;
        webview.loadUrl(urlQuery);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_query, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.menu_back:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyWebView extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
