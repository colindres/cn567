package com.example.apple02.icp03;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.tag;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static android.os.Build.VERSION_CODES.N;
import static android.widget.GridLayout.spec;
import static com.example.apple02.icp03.R.id.gridlayout_01;
import static com.example.apple02.icp03.R.id.tv01;

public class MainActivity extends AppCompatActivity {

    String TAG = "Result";

    ImageButton imgbtns[][] = new ImageButton[3][3];
    int[][] mButtons = new int[][]{{R.drawable.bg, R.drawable.bg, R.drawable.bg},
            {R.drawable.bg, R.drawable.bg, R.drawable.bg},
            {R.drawable.bg, R.drawable.bg, R.drawable.bg}};
    int tagcount = 0;

    String[][] gameSet = new String[3][3];
    String x = "x";
    String y = "y";

    boolean xChoosen = false;
    int count = 0;
    boolean gamestarted = false;
    String[] choice = {"X", "O"};
    boolean startX = false;
    TextView tv01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(LinearLayout.HORIZONTAL);
        linearLayout.setPadding(25, 25, 25, 25);

        tv01 = new TextView(this);
        tv01.setText("Click to start game");
        tv01.setTextSize(22);
        linearLayout.addView(tv01);

        final GridLayout gridLayout = new GridLayout(this);
        gridLayout.setAlignmentMode(GridLayout.ALIGN_BOUNDS);

        int total = 9;
        int column = 3;
        int row = 3;

        gridLayout.setColumnCount(column);
        gridLayout.setRowCount(row);

//        GridLayout.Spec titleTxtSpecColum = GridLayout.spec(2, GridLayout.BASELINE);
//        GridLayout.Spec titleRowSpec = GridLayout.spec(0);
//        gridLayout.addView(tv01, new GridLayout.LayoutParams(titleRowSpec, titleTxtSpecColum));

//        GridLayout.Spec row1 = spec(0);
//        GridLayout.Spec row2 = spec(1);
//        GridLayout.Spec row3 = spec(2);
//
//        GridLayout.Spec col1 = spec(0);
//        GridLayout.Spec col2 = spec(1);
//        GridLayout.Spec col3 = spec(2);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                imgbtns[i][j] = new ImageButton(MainActivity.this);
                imgbtns[i][j].setImageResource(mButtons[i][j]);
                GridLayout.LayoutParams params1 = new GridLayout.LayoutParams();
                params1.width = 300;
                params1.height = 300;
                params1.columnSpec = GridLayout.spec(j, 1);
                params1.rowSpec = GridLayout.spec(i, 1);
                imgbtns[i][j].setScaleType(ImageView.ScaleType.FIT_CENTER);
                imgbtns[i][j].setOnClickListener(new MyClickListener(j, i));
                imgbtns[i][j].setTag(tagcount++);
                imgbtns[i][j].setOnClickListener(new MyClickListener(j, i));
                gridLayout.addView(imgbtns[i][j], params1);
            }
        }




        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose X or O")
                .setItems(choice, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            // X is selected
                            case 0:
                                tv01.setText("Player 1 choose X");
                                Toast.makeText(MainActivity.this, "You chose: " + choice[which], Toast.LENGTH_SHORT).show();
                                xChoosen = true;
                                break;

                            //  O is selected
                            case 1:
                                xChoosen = false;
                                tv01.setText("Player 1 choose O");
                                Toast.makeText(MainActivity.this, "You chose: " + choice[which], Toast.LENGTH_SHORT).show();
                                break;

                        }

                        gamestarted = true;
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();

        linearLayout.addView(gridLayout);
        setContentView(linearLayout);
    }

    //---- Game Finished shows dialog
    public void isGameFinish() {
        count++;
        if (count == 9) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Game finished")
                    .setMessage("You finished the game!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
        checkPlayerXWin();

    }

    //--- Show changes the image to play
    public boolean setImage(ImageButton imgbtn, String string) {

        if (xChoosen == true) {
            setX(imgbtn);
            string = x;
        } else {
            setY(imgbtn);
            string = y;
        }

        return xChoosen;
    }

    public void setX(ImageButton imgbtn) {
        imgbtn.setImageResource(R.drawable.tac);
        tv01.setText("Turn for Player O");
        xChoosen = false;
    }

    public void setY(ImageButton imgbtn) {
        imgbtn.setImageResource(R.drawable.tic);
        tv01.setText("Turn for Player X");
        xChoosen = true;
    }
    public void checkPlayerXWin(){

        if( gameSet[0][1] == x & gameSet[0][0] == x & gameSet[0][1] == x){
            tv01.setText("Play X wins");
        }

        if( gameSet[0][1] == x & gameSet[1][0] == x & gameSet[1][0] == x){
            tv01.setText("Play X wins");
        }


        if( gameSet[0][1] == x & gameSet[1][1] == x & gameSet[2][2] == x){
            tv01.setText("Play X wins");
        }
    }


    public class MyClickListener implements View.OnClickListener {

        int x, y;

        public MyClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void onClick(View v) {
            // final ImageButton imgbtn = (ImageButton) findViewById(v.getId());

            int imgTag = (int) v.getTag();
            Log.i(TAG, String.valueOf(imgTag));

            if (gamestarted)
                switch (imgTag) {
                    case 0:
                        setImage(imgbtns[0][0], gameSet[0][1]);
                        break;
                    case 1:
                        setImage(imgbtns[0][1], gameSet[0][1]);
                        break;
                    case 2:
                        setImage(imgbtns[0][2], gameSet[0][2]);
                        break;
                    case 3:
                        setImage(imgbtns[1][0], gameSet[1][0]);
                        break;
                    case 4:
                        setImage(imgbtns[1][1], gameSet[1][1]);
                        break;
                    case 5:
                        setImage(imgbtns[1][2], gameSet[1][2]);
                        break;
                    case 6:
                        setImage(imgbtns[2][0], gameSet[2][0]);
                        break;
                    case 7:
                        setImage(imgbtns[2][1], gameSet[2][1]);
                        break;
                    case 8:
                        setImage(imgbtns[2][2], gameSet[2][2]);
                        break;
                }

            isGameFinish();
        }
    }

}
