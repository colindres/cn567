package app.com.yadia.intent2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button btn01;
    EditText et_weight, et_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_height = (EditText) findViewById(R.id.et_Height);
        et_weight = (EditText) findViewById(R.id.et_Weight);

        btn01 = (Button) findViewById(R.id.btn01);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.setClass(MainActivity.this, SecondActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("HEIGHT", et_height.getText().toString());
                bundle.putString("WEIGHT", et_weight.getText().toString());
                intent1.putExtras(bundle);
                startActivity(intent1);
            }
        });

    }
}
