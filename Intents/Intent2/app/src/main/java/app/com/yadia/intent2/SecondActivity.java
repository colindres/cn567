package app.com.yadia.intent2;

import android.renderscript.Double2;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    Button btn01;
    String height, weight;
    TextView tv01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv01 = (TextView) findViewById(R.id.tv_bmi);
        btn01 = (Button) findViewById(R.id.btn_return);

        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
           tv01.setText( bundle.get("HEIGHT") + "--" + bundle.getString("WEIGHT"));
        }

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
