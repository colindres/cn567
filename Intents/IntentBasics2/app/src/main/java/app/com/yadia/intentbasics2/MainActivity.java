package app.com.yadia.intentbasics2;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btn_calculate, btn02;
    EditText et_weight, et_height;
    TextView tv03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_height = (EditText) findViewById(R.id.et_height);
        et_weight = (EditText) findViewById(R.id.et_weight);
        btn_calculate = (Button) findViewById(R.id.btn_Calculate);
        tv03 = (TextView) findViewById(R.id.tv03);

        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(MainActivity.this, SecActivity.class );
                Bundle bundle = new Bundle();
                bundle.putString("HEIGHT", et_height.getText().toString());
                bundle.putString("WEIGHT", et_weight.getText().toString());
                myIntent.putExtras(bundle);
                //startActivity(myIntent;
                startActivityForResult( myIntent, 12);

            }
        });

        btn02 = (Button) findViewById(R.id.btn02);
        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(Intent.ACTION_WEB_SEARCH);
                searchIntent.putExtra(SearchManager.QUERY, "Android");
                startActivity(searchIntent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 12:
                if(resultCode == 1)
                    tv03.setText(data.getStringExtra("TT"));
                break;

        }



    }
}
