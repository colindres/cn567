package app.com.yadia.intentbasics2;

import android.content.Intent;
import android.renderscript.Double2;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static java.lang.Double.parseDouble;

public class SecActivity extends AppCompatActivity {

    Button btn01;
    TextView tv01;
    Double rr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec);

        tv01 = (TextView) findViewById(R.id.tv_bmi);
        btn01 = (Button) findViewById(R.id.btn_return);

        Bundle bundle = this.getIntent().getExtras();
        tv01.setText( bundle.get("HEIGHT") + " -- " + bundle.getString("WEIGHT"));
        rr = Double.parseDouble(bundle.getString("HEIGHT")) - Double.parseDouble(bundle.getString("WEIGHT")) ;

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                replyIntent.putExtra("TT", rr.toString());
                setResult(1, replyIntent );
                finish();
            }
        });

    }
}
