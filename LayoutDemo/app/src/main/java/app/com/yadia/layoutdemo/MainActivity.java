package app.com.yadia.layoutdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    TableLayout tableLayout;
    TableRow tableRow01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        TextView lbl101 = new TextView(this);
        lbl101.setText("Testing");
        lbl101.setTextSize(20);
        lbl101.setGravity(Gravity.CENTER);

        setContentView(linearLayout);


        Button btn01 = new Button(this);
        btn01.setText("Button");

        linearLayout.addView(lbl101);
        linearLayout.addView(btn01);
    }
}
