package app.com.yadia.customlistadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private ListView lv;
    Weather[] weatherData;
    WeatherAdapter weatherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.listview01);

        weatherData = new Weather[]{
                new Weather(R.drawable.cloudy, "Cloudy"),
                new Weather(R.drawable.sunny, "Sunny"),
                new Weather(R.drawable.rainy, "Rainy")

        };
        //Adapter
        weatherAdapter = new WeatherAdapter(this,R.layout.listview_item_row, weatherData);
        View holder = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);

        lv.addHeaderView(holder);
        lv.setAdapter(weatherAdapter);
    }
}
