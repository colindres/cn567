package app.com.yadia.customlistadapter;

/**
 * Created by yadia on 12/5/16.
 */

public class Weather {
    private int icon;
    private String title;

    public Weather(){ //no arguments
        super();
    }

    public Weather(int myIcon, String mytitle){
        super();
        this.icon = myIcon;
        this.title = mytitle;
    }

    public int getIcon(){
        return icon;
    }

    public String getTitle(){
        return title;
    }
}
