package app.com.yadia.customlistadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yadia on 12/5/16.
 */

public class WeatherAdapter extends ArrayAdapter<Weather> {

    Context context;
    int layoutResourceId;
    Weather data[] = null;

    public WeatherAdapter(Context context, int resource, Weather[] data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.data = data;
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        WeatherHolder holder = null;

        if (convertView == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new WeatherHolder();
            holder.imgIcon = (ImageView) row.findViewById(R.id.imageView_item);
            holder.txtTile = (TextView) row.findViewById(R.id.textView_item);
            row.setTag(holder);

        } else {
            holder = (WeatherHolder) row.getTag();
        }

        Weather wea = data[position];
        holder.txtTile.setText(wea.getTitle());
        holder.imgIcon.setImageResource(wea.getIcon());

        return row;
    }

    static class WeatherHolder{
        ImageView imgIcon;
        TextView txtTile;
    }
}
