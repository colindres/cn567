package app.com.yadia.spinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    String[] days;
    TextView tv01;
    List dayList = new ArrayList();
    String selStr;
    ArrayAdapter<String> a1, a2;
    ListView listView;
    Button btn01;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner) findViewById(R.id.spinner);
        tv01  = (TextView) findViewById(R.id.textView01);
        listView = (ListView) findViewById(R.id.listview01);
        btn01 = (Button) findViewById(R.id.button01);
        days = getResources().getStringArray(R.array.days);

        for(String str:getResources().getStringArray(R.array.days))
            dayList.add(str);

        a1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dayList);
        spinner.setAdapter(a1);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selStr =  parent.getSelectedItem().toString();
                Toast.makeText(MainActivity.this, parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                tv01.setText(parent.getSelectedItem().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tv01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dayList.remove(selStr);
                a1.notifyDataSetChanged();
            }
        });
        a2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, days);
        listView.setAdapter(a2);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ss = "";
                for(int i = 0; i < days.length; i++ )
                    if(listView.isItemChecked(i))
                        ss = ss + days[i] + " ";
                Toast.makeText(MainActivity.this, ss, Toast.LENGTH_SHORT).show();
            }
        });

        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selStr =  parent.getSelectedItem().toString();
                tv01.setText(parent.getSelectedItem().toString());
                Toast.makeText(MainActivity.this,parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
