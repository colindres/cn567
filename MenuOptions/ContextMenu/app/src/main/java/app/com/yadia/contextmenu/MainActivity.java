package app.com.yadia.contextmenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    RelativeLayout rl_main;
    TextView tv01;
    ActionMode mActionMode;
    String TAG = "ContextMenu";
    ImageView imageView01;
    PopupMenu popupMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        registerForContextMenu(rl_main);
        tv01 = (TextView) findViewById(R.id.tv01);
        tv01.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                if(mActionMode !=null)
//                    return false;

                mActionMode = MainActivity.this.startActionMode(mActionModeCallback);
                v.setSelected(true);
                return true;
            }
        });

        imageView01 = (ImageView) findViewById(R.id.imageView01);
        popupMenu = new PopupMenu(this, imageView01);
        popupMenu.inflate(R.menu.menu_popup);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menupop_Red:
                        Toast.makeText(MainActivity.this, "Color RED", Toast.LENGTH_SHORT).show();
                        imageView01.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        return true;
                    case R.id.menupop_Green:
                        Toast.makeText(MainActivity.this, "Color GREEN", Toast.LENGTH_SHORT).show();
                        imageView01.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        return true;
                    default:
                        return false;
                }
            }
        });

        imageView01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu.show();

            }
        });


    }
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback(){

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.menu_02, menu);
            //return false;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.d(TAG, "ActionITemClicked");

            switch (item.getItemId()){
                case R.id.menuContext_Red:
                    Toast.makeText(MainActivity.this, "Color RED", Toast.LENGTH_SHORT).show();
                    rl_main.setBackgroundColor(getResources().getColor(R.color.colorRed));
                    return true;
                case R.id.menuContext_Green:
                    Toast.makeText(MainActivity.this, "Color GREEN", Toast.LENGTH_SHORT).show();
                    rl_main.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;

        }
    };

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo info) {
        super.onCreateContextMenu(menu, v, info);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_02, menu);
    }

    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)
                item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.menuContext_Red:
                Toast.makeText(this, "Color RED", Toast.LENGTH_SHORT).show();
                rl_main.setBackgroundColor(getResources().getColor(R.color.colorRed));
                return true;
            case R.id.menuContext_Green:
                Toast.makeText(this, "Color GREEN", Toast.LENGTH_SHORT).show();
                rl_main.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_01, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.menu_settings:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}