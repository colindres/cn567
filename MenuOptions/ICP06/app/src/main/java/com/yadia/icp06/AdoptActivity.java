package com.yadia.icp06;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdoptActivity extends AppCompatActivity {

    WebView webView;
    EditText et_query;
    Button search;
    String query, url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adopt);
        webView = (WebView) findViewById(R.id.webview_adopt);
        webView.setWebViewClient(new MyWebView());
        webView.setVisibility(View.INVISIBLE);
        et_query = (EditText) findViewById(R.id.et_query);
        search = (Button) findViewById(R.id.btn_search);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query = et_query.getText().toString();
                webView.setVisibility(View.VISIBLE);
                runSearch(query);
            }
        });

    }

    public void runSearch(String myquery){
        url = "https://www.google.com/search?q=" + "adopt taiwan " + myquery;
        webView.loadUrl(url);
    }

    public class MyWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets")
                .setIcon(R.drawable.ic_pets_white_24dp);
        subMenu.add(Menu.NONE, 110, 0, "Cats");
        subMenu.add(Menu.NONE, 120, 0, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 100:
                Toast.makeText(this, "Pick a pet to see", Toast.LENGTH_SHORT).show();
                return true;
            case 110:
                Intent intent01 = new Intent(this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(this, ThirdActivity.class);
                startActivity(intent02);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
