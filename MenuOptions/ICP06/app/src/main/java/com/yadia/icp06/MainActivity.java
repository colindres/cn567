package com.yadia.icp06;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets");
        subMenu.add(Menu.NONE, 110, 100, "Cat");
        subMenu.add(Menu.NONE, 120, 200, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(MainActivity.this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 100:
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 110:
                Intent intent04 = new Intent(this, SecondActivity.class);
                startActivity(intent04);
                return true;
            case 220:
                Intent intent01 = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(MainActivity.this, ThirdActivity.class);
                startActivity(intent02);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
