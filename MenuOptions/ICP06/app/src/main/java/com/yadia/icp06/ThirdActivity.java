package com.yadia.icp06;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

public class ThirdActivity extends AppCompatActivity {

    SparkButton heart;
    WebView webViewDog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

     final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_dog)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = Uri.parse("https://www.google.com/search?q=dog+adoptions");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        builder.create();
        // Create the AlertDialog object and return it

        heart = (SparkButton) findViewById(R.id.sbtn_heartDog);
        heart.playAnimation();
        heart.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                if (buttonState){
                    builder.show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets")
                .setIcon(R.drawable.ic_pets_white_24dp);
        subMenu.add(Menu.NONE, 110, 0, "Cats");
        subMenu.add(Menu.NONE, 120, 0, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 100:
                return true;
            case 110:
                Intent intent01 = new Intent(this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(this, ThirdActivity.class);
                startActivity(intent02);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
