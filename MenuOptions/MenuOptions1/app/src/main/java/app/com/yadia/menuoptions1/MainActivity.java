package app.com.yadia.menuoptions1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rl = (RelativeLayout) findViewById(R.id.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_01, menu);
//        MenuItem item = menu.add(0, 50, 300, "About");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 50, 0, "Colors")
                .setIcon(R.drawable.ic_invert_colors_white_24dp);
        subMenu.add(Menu.NONE, 110, 10, "Red");
        subMenu.add(Menu.NONE, 120, 20, "Green");
        subMenu.add(Menu.NONE, 130, 30, "Blue");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:
                Toast.makeText(this, "Setings option", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_help:
                Toast.makeText(this, "Menu options", Toast.LENGTH_SHORT).show();
                return true;
            case 50:
                Toast.makeText(this, "Colors!", Toast.LENGTH_SHORT).show();
                return true;
            case 110:
                rl.setBackgroundColor(getResources().getColor(R.color.colorRed));
                return true;
            case 120:
                rl.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                return true;
            case 130:
                rl.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
