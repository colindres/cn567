package app.com.yadia.dialogdemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity {

    Button btnConfirm, btn02, btn03;
    String[] options = {"A", "B", "C", "D"};
    String[] colors = {"Red", "Yellow", "Green", "Blue"};
    String[] items = {"Android ", "iOS ", "Windows Mobile "};
    boolean[] itemsChecked = new boolean[items.length];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        btnConfirm = (Button) findViewById(R.id.btn_alert);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Confirmation")
                        .setMessage("Are you sure?")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, "Ok", Toast.LENGTH_SHORT).show();

                            }
                        })
                        .show();

            }
        });

        btn02 = (Button) findViewById(R.id.btn02);
        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Single Choice")
                        .setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        btn02.setBackgroundColor(getResources().getColor(R.color.colorRed));
                                        break;
                                    case 1:
                                        btn02.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                                        break;
                                    case 2:
                                        btn02.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                                        break;
                                    case 3:
                                        btn02.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                                        break;

                                }
                                Toast.makeText(MainActivity.this, colors[which], Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this, "Ok", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();

            }
        });

        btn03 = (Button) findViewById(R.id.btn03);
        btn03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Multiple Choice")
                        .setMultiChoiceItems(items, itemsChecked, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                Toast.makeText(MainActivity.this, items[which] + (isChecked? "checked" : "not checked"),
                                        Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String msg = " ";
                                for(int index = 0; index < 3; index++) {
                                    if (itemsChecked[which])
                                        msg = msg + items[which];
                                }

                                Toast.makeText(MainActivity.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            }
        });




    }
}
