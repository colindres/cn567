package app.com.yadia.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.R.attr.key;
import static android.R.id.primary;

/**
 * Created by yadia on 12/6/16.
 */

public class MyDBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "MyContacts";
    private static final int DB_VERSION = 1;

    public MyDBHelper(Context context){
        super(context, DB_NAME, null,  DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLLE  titles (_id integer PRIMARY KEY autoincrement, " +
        "item text no null, price real no null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS titles" );
        onCreate(db);
    }
}
