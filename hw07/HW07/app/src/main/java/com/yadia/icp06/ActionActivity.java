package com.yadia.icp06;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ActionActivity extends AppCompatActivity {

    LinearLayout ll;
    ImageButton imgcat, imgDog;
    Button helpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        imgcat = (ImageButton) findViewById(R.id.imgbutton_Cat);
        imgDog = (ImageButton) findViewById(R.id.imgButton_dog);
        helpBtn = (Button) findViewById(R.id.button_help);
        ll = (LinearLayout) findViewById(R.id.ll_pets);
        ll.setVisibility(View.INVISIBLE);

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.setVisibility(View.VISIBLE);
                Toast.makeText(ActionActivity.this, "Hope this helps!", Toast.LENGTH_SHORT).show();
                imgcat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent01 = new Intent(ActionActivity.this, SecondActivity.class);
                        startActivity(intent01);
                    }
                });

                imgDog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent01 = new Intent(ActionActivity.this, ThirdActivity.class);
                        startActivity(intent01);
                    }
                });
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets")
                .setIcon(R.drawable.ic_pets_white_24dp);
        subMenu.add(Menu.NONE, 110, 0, "Cats");
        subMenu.add(Menu.NONE, 120, 0, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(ActionActivity.this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 100:
                Toast.makeText(this, "Pick a pet to see", Toast.LENGTH_SHORT).show();
                return true;
            case 110:
                Intent intent01 = new Intent(ActionActivity.this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(ActionActivity.this, ThirdActivity.class);
                startActivity(intent02);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
