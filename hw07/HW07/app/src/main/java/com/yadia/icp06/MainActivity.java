package com.yadia.icp06;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    ActionMode mActionMode;
    TextView tvmain, tvchat01, tvchat02;
    ImageView imageView01, imageView02;
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvmain = (TextView) findViewById(R.id.txtInfo);
        tvchat01 = (TextView) findViewById(R.id.txtChat01);
        tvchat02 = (TextView) findViewById(R.id.txtChat02);
        imageView01 = (ImageView) findViewById(R.id.img_01);
        imageView02 = (ImageView) findViewById(R.id.img_02);
        rl = (RelativeLayout) findViewById(R.id.activity_main);


        tvmain.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //return false;
                mActionMode = MainActivity.this.startActionMode(mActionModeCallback);
                v.setSelected(true);
                return true;

            }
        });
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback(){

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()){
                case R.id.menu_context_dog:
                    tvchat01.setText(R.string.txt_dog2);
                    tvchat02.setText(R.string.dog_txt);
                    imageView01.setImageResource(R.drawable.dog2);
                    imageView02.setImageResource(R.drawable.dog);
                    rl.setBackgroundResource(R.drawable.grass);

                    return true;
                case R.id.menu_context_cat:
                    tvchat01.setText(R.string.cat_txt);
                    tvchat02.setText(R.string.txt_cat2);
                    imageView01.setImageResource(R.drawable.cat);
                    imageView02.setImageResource(R.drawable.cat2);
                    rl.setBackgroundResource(R.drawable.sand);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_context_dog:
                return true;
            case R.id.menu_context_cat:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets");
        subMenu.add(Menu.NONE, 110, 100, "Cat");
        subMenu.add(Menu.NONE, 120, 200, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(MainActivity.this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 100:
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 110:
                Intent intent04 = new Intent(this, SecondActivity.class);
                startActivity(intent04);
                return true;
            case 220:
                Intent intent01 = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(MainActivity.this, ThirdActivity.class);
                startActivity(intent02);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
