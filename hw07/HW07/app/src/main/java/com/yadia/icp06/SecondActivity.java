package com.yadia.icp06;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

public class SecondActivity extends AppCompatActivity {

    SparkButton heart;
    RadioGroup rg;
    RadioButton rbCatFacts, rbCatBreeds, rbCatPics;
    WebView webViewCats;
    Button clear;
    PopupMenu popupMenu;
    ImageView imgCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        webViewCats = (WebView) findViewById(R.id.webview_cats);
        clear = (Button) findViewById(R.id.btn_clear);
        rg = (RadioGroup) findViewById(R.id.radiogroup_Cats);
        rbCatBreeds = (RadioButton) findViewById(R.id.rb_catBreeds);
        rbCatFacts = (RadioButton) findViewById(R.id.rb_catFacts);
        rbCatPics = (RadioButton) findViewById(R.id.rb_catPics);
        imgCat = (ImageView) findViewById(R.id.imageView_cat);

        popupMenu = new PopupMenu(this, imgCat);
        popupMenu.inflate(R.menu.menu_ppm);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_ppm_cat01:
                        imgCat.setImageResource(R.drawable.cat2);
                        return true;
                    case R.id.menu_ppm_cat02:
                        imgCat.setImageResource(R.drawable.cat03);
                        return true;
                    case R.id.menu_ppm_cat03:
                        imgCat.setImageResource(R.drawable.cat);
                        return true;
                    default:
                        return false;
                }
            }
        });

        imgCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu.show();
            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.adopt_cat)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        webViewCats.setVisibility(View.VISIBLE);
                        runSearch("Cat Adoptions");
                    }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        builder.create();

        heart = (SparkButton) findViewById(R.id.sbtn_heartCats);
        heart.playAnimation();
        heart.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                if (buttonState){
                    builder.show();
                }
            }
        });

        webViewCats.setWebViewClient(new MyWebView());
        webViewCats.setVisibility(View.INVISIBLE);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                webViewCats.setVisibility(View.VISIBLE);
                switch (checkedId){
                    case R.id.rb_catFacts:
                        runSearch("fun cat facts");
                        break;
                    case R.id.rb_catBreeds:
                        runSearch("cat breeds");
                        break;
                    case R.id.rb_catPics:
                        webViewCats.loadUrl("http://www.google.com/search?q=cats&tbm=isch");
                        break;
                }
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewCats.setVisibility(View.INVISIBLE);
                rg.clearCheck();
            }
        });




    }

    public void runSearch(String myquery){
        String url = "https://www.google.com/search?q="  + myquery;
        webViewCats.loadUrl(url);
    }

    public class MyWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets");
        subMenu.add(Menu.NONE, 110, 0, "Cats");
        subMenu.add(Menu.NONE, 120, 0, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent (SecondActivity.this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 100:
                Toast.makeText(this, "Pick a pet to see", Toast.LENGTH_SHORT).show();
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 110:
                Intent intent01 = new Intent(SecondActivity.this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(SecondActivity.this, ThirdActivity.class);
                startActivity(intent02);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
