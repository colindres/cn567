package com.yadia.icp06;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class ThirdActivity extends AppCompatActivity {

    SparkButton heart;
    ListView lvDog;
    ArrayAdapter<String> a1;
    String[] dogBreeds;
    String breed;
    ImageView doggy;
    String TAG = "DOGACTIVITY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        doggy = (ImageView) findViewById(R.id.imageViewDoggy);

     final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_dog)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = Uri.parse("https://www.google.com/search?q=dog+adoptions");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        builder.create();
        // Create the AlertDialog object and return it

        heart = (SparkButton) findViewById(R.id.sbtn_heartDog);
        heart.playAnimation();
        heart.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                if (buttonState){
                    builder.show();
                }
            }
        });
        dogBreeds = getResources().getStringArray(R.array.dogBreeds);
        lvDog = (ListView) findViewById(R.id.listview_dogs);
        a1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, dogBreeds);
        lvDog.setAdapter(a1);

        lvDog.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvDog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                breed = parent.getSelectedItem().toString();
                //Log.i(TAG, breed);
                //Toast.makeText(ThirdActivity.this,parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        lvDog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) lvDog.getItemAtPosition(position);
                showDogPick(position);
                Toast.makeText(ThirdActivity.this, "ListItem: " + position, Toast.LENGTH_SHORT).show();
               Log.i(TAG, value);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_options, menu);
        menu.add(Menu.NONE, 06, 110, "Adopt");
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, 100, 100, "Pets");
        subMenu.add(Menu.NONE, 110, 0, "Cats");
        subMenu.add(Menu.NONE, 120, 0, "Dogs");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        switch (id){
            case R.id.menu_action1:
                Intent intent = new Intent(this, ActionActivity.class);
                startActivity(intent);
                return true;
            case 06:
                Intent intent03 = new Intent(this, AdoptActivity.class);
                startActivity(intent03);
                return true;
            case 100:
                return true;
            case 110:
                Intent intent01 = new Intent(this, SecondActivity.class);
                startActivity(intent01);
                return true;
            case 120:
                Intent intent02 = new Intent(this, ThirdActivity.class);
                startActivity(intent02);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDogPick(int doggoValue){
        switch (doggoValue){
            case 0:
                doggy.setImageResource(R.drawable.pug);
                break;
            case 1:
                doggy.setImageResource(R.drawable.cairn_terrier);
                break;
            case 2:
                doggy.setImageResource(R.drawable.american_eskimo);
                break;
            case 3:
                doggy.setImageResource(R.drawable.chihuahua);
                break;
            case 4:
                doggy.setImageResource(R.drawable.bolognese);
                break;
            case 5:
                doggy.setImageResource(R.drawable.german_pinsche);
                break;
            case 6:
                doggy.setImageResource(R.drawable.dog);
                break;
            default:
                doggy.setImageResource(R.drawable.dog);
                break;
        }
    }
}
